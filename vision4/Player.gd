extends CharacterBody2D

@export_range(10, 200) var speed: float = 40
@export_range(10, 200) var rotation_speed: float = 30
var target: Vector2 = Vector2.ZERO
var cango: bool = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _draw():
	draw_rect(Rect2(-10, -10, 20, 20), Color.DARK_RED)

func _input(event):
	if event.is_action_pressed("move_to"):
		cango = true
	elif event.is_action_released("move_to"):
		cango = false

func _physics_process(delta):
	if cango:
		target = get_local_mouse_position()
		
	rotation = target.angle() * rotation_speed * delta
	velocity = Vector2(speed, 0).rotated(rotation)
	move_and_slide()


func _get_direction(target: Vector2) -> Vector2:
	return get_global_transform().origin.direction_to(target)


func _on_view_area_entity_found(entity):
	print("Entity found: ", entity)
	print($ViewArea.get_entities())
