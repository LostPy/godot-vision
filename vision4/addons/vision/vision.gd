@tool
extends EditorPlugin


func _enter_tree():
	add_custom_type(
		"ViewArea2D",
		"Node2D",
		preload("res://addons/vision/view-area/view-area.gd"),
		preload("res://addons/vision/view-area/view-area-icon.svg")
	)
	add_custom_type(
		"FieldOfView2D",
		"Node2D",
		preload("res://addons/vision/field-of-view/fov.gd"),
		preload("res://addons/vision/field-of-view/fov-icon.svg"),
	)

func _exit_tree():
	remove_custom_type("ViewArea")
	remove_custom_type("FieldOfView2D")
