@tool
extends Node2D

@export_range(1, 360) var view_angle: float = 60.0
@export_range(5, 1000) var view_distance: float = 100.0
@export_range(1, 360) var view_details: float = 60.0
@export var view_with_areas: bool = false
@export var view_with_bodies: bool = true
@export_flags_2d_physics var physics_layer: int = 0xFFFFFF

@export_category("Debug parameters")
@export var show_fov: bool = false
@export var fov_color: Color = Color("#b23d7f74")

var rid_excluded: Array[RID] = []
var in_view: Array = []

# Buffer to target points
var points_arc: Array = []



func _exit_tree():
	in_view.clear()


func _ready():
	for i in get_parent().get_child_count():
		var child = get_parent().get_child(i)
		if is_instance_of(child, CollisionObject2D):
			rid_excluded.append(child.get_rid())


func _physics_process(delta):
	check_view()
	queue_redraw()


func _draw():
	if show_fov:
		draw_fov()


func draw_fov():
	for point in points_arc:
		draw_line(transform.origin, point, fov_color)


func _vec_from_angle(angle: float) -> Vector2:
	return Vector2(cos(angle), sin(angle))


func check_view() -> void:
	var dir_angle = transform.get_rotation()
	var start_angle = dir_angle - deg_to_rad(view_angle / 2)
	var end_angle = dir_angle + deg_to_rad(view_angle / 2)

	points_arc.clear()
	in_view.clear()

	var space_state = get_world_2d().direct_space_state

	for deg_angle in range(rad_to_deg(start_angle), rad_to_deg(end_angle), view_angle / view_details):
		var current_angle = deg_to_rad(deg_angle)
		var target = transform.origin + _vec_from_angle(current_angle) * view_distance

		var request = PhysicsRayQueryParameters2D.create(
			get_global_transform().origin,
			to_global(target),
			physics_layer,
			rid_excluded
		)
		request.collide_with_areas = view_with_areas
		request.collide_with_bodies = view_with_bodies
		var result = space_state.intersect_ray(request)

		if result.is_empty():
			points_arc.append(target)
		else:
			points_arc.append(to_local(result["position"]))
			in_view.append(result.collider)
