@tool
extends Area2D

var _entities = {}
signal entity_found(entity)


func _ready():
	connect("area_entered", _on_area_entered)
	connect("area_exited", _on_area_exited)
	connect("body_entered", _on_body_entered)
	connect("body_exited", _on_body_exited)

func get_entities() -> Array:
	return _entities.values()


func _add_object(object: Node2D):
	var entity = object.get_parent()
	_entities[hash(entity)] = entity
	emit_signal("entity_found", entity)


func _remove_object(object: Node2D):
	_entities.erase(hash(object.get_parent()))


func _on_area_entered(area: Area2D):
	_add_object(area)


func _on_body_entered(body: Node2D):
	_add_object(body)


func _on_area_exited(area: Area2D):
	_remove_object(area)


func _on_body_exited(body: Node2D):
	_remove_object(body)
